scalaVersion := "2.12.10"

scalacOptions ++= Seq(
  "-Xlog-reflective-calls",
  "-opt:_",
  "-deprecation",                      // Emit warning and location for usages of deprecated APIs.
  "-encoding", "utf-8",                // Specify character encoding used by source files.
  "-explaintypes",                     // Explain type errors in more detail.
  "-feature",                          // Emit warning and location for usages of features that should be imported explicitly.
  "-language:existentials",            // Existential types (besides wildcard types) can be written and inferred
  "-language:experimental.macros",     // Allow macro definition (besides implementation and application)
  "-language:higherKinds",             // Allow higher-kinded types
  "-language:implicitConversions",     // Allow definition of implicit functions called views
  "-language:postfixOps",
  "-unchecked",                        // Enable additional warnings where generated code depends on assumptions.
  "-Xcheckinit",                       // Wrap field accessors to throw an exception on uninitialized access.
  "-Xfuture",                          // Turn on future language features.
  "-Xlint:adapted-args",               // Warn if an argument list is modified to match the receiver.
  "-Xlint:by-name-right-associative",  // By-name parameter of right associative operator.
  "-Xlint:delayedinit-select",         // Selecting member of DelayedInit.
  "-Xlint:doc-detached",               // A Scaladoc comment appears to be detached from its element.
  "-Xlint:inaccessible",               // Warn about inaccessible types in method signatures.
  "-Xlint:infer-any",                  // Warn when a type argument is inferred to be `Any`.
  "-Xlint:missing-interpolator",       // A string literal appears to be missing an interpolator id.
  "-Xlint:nullary-override",           // Warn when non-nullary `def f()' overrides nullary `def f'.
  "-Xlint:nullary-unit",               // Warn when nullary methods return Unit.
  "-Xlint:option-implicit",            // Option.apply used implicit view.
  "-Xlint:package-object-classes",     // Class or object defined in package object.
  "-Xlint:poly-implicit-overload",     // Parameterized overloaded implicit methods are not visible as view bounds.
  "-Xlint:private-shadow",             // A private field (or class parameter) shadows a superclass field.
  "-Xlint:stars-align",                // Pattern sequence wildcard must align with sequence component.
  "-Xlint:type-parameter-shadow",      // A local type parameter shadows a type already in scope.
  "-Xlint:unsound-match",              // Pattern match may not be typesafe.
  "-Yno-adapted-args",                 // Do not adapt an argument list (either by inserting () or creating a tuple) to match the receiver.
  "-Ypartial-unification",             // Enable partial unification in type constructor inference
  "-Ywarn-dead-code",                  // Warn when dead code is identified.
  "-Ywarn-inaccessible",               // Warn about inaccessible types in method signatures.
  "-Ywarn-infer-any",                  // Warn when a type argument is inferred to be `Any`.
  "-Ywarn-nullary-override",           // Warn when non-nullary `def f()' overrides nullary `def f'.
  "-Ywarn-nullary-unit",               // Warn when nullary methods return Unit.
  "-Ywarn-numeric-widen",              // Warn when numerics are widened.
  "-Ywarn-value-discard",              // Warn when non-Unit expression results are unused.
  "-Xlint:constant",                   // Evaluation of a constant arithmetic expression results in an error.
  "-Ywarn-unused:imports",             // Warn if an import selector is not referenced.
  "-Ywarn-unused:locals",              // Warn if a local definition is unused.
  "-Ywarn-unused:params",              // Warn if a value parameter is unused.
  "-Ywarn-unused:patvars",             // Warn if a variable bound in a pattern is unused.
  "-Ywarn-unused:privates",            // Warn if a private member is unused.
  "-Ywarn-unused:implicits",           // Warn if an implicit parameter is unused.
  "-Ywarn-extra-implicit",             // Warn when more than one implicit parameter section is defined.
  "-Yrangepos"                         // required by SemanticDB compiler plugin
)

val versions = new {
  val Http4sVersion = "0.20.17"
  val CirceVersion = "0.12.3"
  val Specs2Version = "4.8.3"
  val LogbackVersion = "1.2.3"
  val DoobieVersion = "0.8.8"
}

lazy val root = (project in file("."))
  .settings(
    organization := "ru.tinkoff.fintech",
    name := "scala-http-server",
    version := "0.1-SNAPSHOT",
    libraryDependencies ++= Seq(
      "org.http4s"      %% "http4s-blaze-server" % versions.Http4sVersion,
      "org.http4s"      %% "http4s-blaze-client" % versions.Http4sVersion,
      "org.http4s"      %% "http4s-circe"        % versions.Http4sVersion,
      "org.http4s"      %% "http4s-dsl"          % versions.Http4sVersion,
      "io.circe"        %% "circe-generic"       % versions.CirceVersion,
      "org.specs2"      %% "specs2-core"         % versions.Specs2Version % "test",
      "ch.qos.logback"  %  "logback-classic"     % versions.LogbackVersion,
      "org.tpolecat"    %% "doobie-core"         % versions.DoobieVersion,
      "org.tpolecat"    %% "doobie-hikari"       % versions.DoobieVersion,
      "org.tpolecat"    %% "doobie-postgres"     % versions.DoobieVersion
    ),
    addCompilerPlugin("org.typelevel" %% "kind-projector"     % "0.11.0" cross CrossVersion.full),
    addCompilerPlugin("com.olegpy"    %% "better-monadic-for" % "0.3.1")
  )

/*
libraryDependencies += "com.softwaremill.sttp.tapir" %% "tapir-core" % "0.12.17"
libraryDependencies += "com.softwaremill.sttp.tapir" %% "tapir-http4s-server" % "0.12.17"
libraryDependencies += "com.softwaremill.sttp.client" %% "core" % "2.0.0-RC6"
libraryDependencies += "org.typelevel" %% "cats-core" % "2.0.0"
libraryDependencies += "org.typelevel" %% "cats-effect" % "2.0.0"
libraryDependencies += "org.http4s" %% "http4s-core" % "0.21.0-RC2"
libraryDependencies += "org.http4s" %% "http4s-dsl" % "0.21.0-RC2"
libraryDependencies += "org.http4s" %% "http4s-server" % "0.21.0-RC2"
libraryDependencies += "org.http4s" %% "http4s-blaze-server" % "0.21.0-RC2"
*/