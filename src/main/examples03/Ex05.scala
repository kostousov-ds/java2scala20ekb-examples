package examples03

object Ex05 {

  case class SomeValidatedData(
                                fieldOne: String,
                                fieldTwo: String
                              )

  object SomeValidatedData {

    def apply(value: String): Option[SomeValidatedData] = value match {
      case "" => None
      case other => Some(SomeValidatedData(other, s"prefix:$other"))
    }
  }

  val v1 = SomeValidatedData("1")
  val v2 = SomeValidatedData("1","1")

}
