package examples03

object Ex06 {

  sealed trait SomeValidatedData {
    def fieldOne: String

    def fieldTwo: String
  }

  object SomeValidatedData {

    val create: String => Option[SomeValidatedData] = {
      case "" => None
      case other => Some(SomeValidatedDataImpl(other, s"prefix:$other"))
    }

    private case class SomeValidatedDataImpl(fieldOne: String, fieldTwo: String) extends SomeValidatedData

  }

//  val v1 = SomeValidatedData.create("1")
//  val v2 = SomeValidatedData.create("1", "1")

}
