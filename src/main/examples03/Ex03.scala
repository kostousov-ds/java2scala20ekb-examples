package examples03

object Ex03 {

  sealed trait Organisation

  case class Person(
                   fio: String,
                   phone: String,
                   position: String
                   ) extends Organisation

  case class Domain(
                   title: String,
                   head: Person,
                   content: List[Organisation]
                   ) extends Organisation

}
