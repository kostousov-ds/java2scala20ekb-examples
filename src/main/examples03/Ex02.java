package examples03;

import java.util.List;

public class Ex02 {

    public interface Organisation{}

    public class Person implements Organisation {
        private final String fio;
        private final String phone;
        private final String position;

        public Person(String fio, String phone, String position) {
            this.fio = fio;
            this.phone = phone;
            this.position = position;
        }
    }

    public class Domain implements Organisation {
        private final String title;
        private final Person head;
        private final List<Organisation> content;

        public Domain(String title, Person head, List<Organisation> content) {
            this.title = title;
            this.head = head;
            this.content = content;
        }
    }

}
