package examples03

import examples03.Ex07.tag.@@

object Ex07 {

  case class Phone1(num: String) extends AnyVal





  object tag {

    trait Tagged[U] {type Tag = U}

    type @@[+T,U] = T with Tagged[U]

  }

  trait PhoneTag
  type PhoneTagged = String @@ PhoneTag
  object Phone2 {
    def apply(str: String): PhoneTagged = str.asInstanceOf[PhoneTagged]
  }

  val p1 = Phone1("phone1")

  val p2 = Phone2("phone2")

//  val p3: Phone2 = "phone3"

}
