package examples03

object Ex09 {

  type SomeType = String
  type AnotherType = Int

  type T1 = Int
  type T2 = String
  type T3 = Boolean

  object One {
    def unapply(value: SomeType): Option[AnotherType] = ???
  }

  object Two {
    def unapply(arg: SomeType): Option[(T1,T2,T3)] = ???
  }

  val test : Any => String = {
    case One(i) => s"$i"
    case Two(t1,t2,_) => s"$t1 -> $t2"
  }
}
