package examples03;

import java.util.List;

public class Ex01 {

    public interface Organisation{}

    public class Person implements Organisation {
        private String fio;
        private String phone;
        private String position;
    }

    public class Domain implements Organisation {
        private String title;
        private String head;
        private List<Organisation> content;
    }

}
