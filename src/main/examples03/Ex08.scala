package examples03

object Ex08 {

  case class Phone1(num: String)
  case class Phone2(num: String) extends AnyVal

  case class Person0(name: String, phone: String)
  case class Person1(name: String, phone: Phone1)
  case class Person2(name: String, phone: Phone2)

  val l0: List[Person0] = List.empty[Person0].filter(_.phone == "+7")
  val l1: List[Person1] = List.empty[Person1].filter(_.phone == "+7")
  val l2: List[Person2] = List.empty[Person2].filter(_.phone == "+7")

}
