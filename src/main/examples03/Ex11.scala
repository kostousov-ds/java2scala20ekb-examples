package examples03

object Ex11 {

  sealed trait WeekDay{
    def name: String
  }

  object WeekDay {
    case object Понедельник extends WeekDay {
      override def name: String = "1"
    }
    case object Вторник extends WeekDay {
      override def name: String = "2"
    }
    case object Среда extends WeekDay {
      override def name: String = "3"
    }
    case object Четверг extends WeekDay {
      override def name: String = "4"
    }
    case object Пятница extends WeekDay {
      override def name: String = "5"
    }
    case object Суббота extends WeekDay {
      override def name: String = "6"
    }
    case object Воскресенье extends WeekDay {
      override def name: String = "7"
    }

    val withName: String => Option[WeekDay] = {
      case "" => Some(???)
      case _ => None
    }
  }

}
