package examples02

import org.slf4j.Logger

import scala.concurrent.{ExecutionContext, Future}

trait BusinessServiceFP2 {

  import BusinessServiceFP2._

  def LOG: Logger

  def masterRepository: EntityRepository[SomeEntityMaster]

  def detailRepository: EntityRepository[SomeEntityDetail]

  def transactor: Transactor

  def asyncHttpClient: AsyncHttpClient

  def timeProvider: () => Long

  implicit def ec: ExecutionContext

  def doSomeBusiness(masterId: Long, detailId: Long): Future[String] = {
    for {
      pairOpt <- Future {
        transactor.runUnsafe { _ =>
          LOG.info(s"REPOSITORY: ${Thread.currentThread.getName}  $masterId/$detailId")
          for {
            m <- masterRepository.findById(masterId)
            d <- detailRepository.findById(detailId)
            _ = LOG.info(s"REPOSITORY: ($m,$d)")
          } yield (m, d)
        }
      }

      result <- pairOpt.fold(
        Future.successful(Result.SUCCESS)
      ) {
        case (master, detail) if isEntitiesShouldBeUpdated(master, detail) =>
          LOG.info(s"BUSINESS: ${Thread.currentThread.getName} ($master,$detail)")
          LOG.info("BUSINESS: before updating")
          for {
            value <- asyncHttpClient.fetchAndPrepareNewFieldValue(master.getId)
            _ = {
              LOG.info(s"UPDATE: ${Thread.currentThread.getName} $value")
              transactor.runUnsafe { _ =>
                val mi = masterRepository.setFieldOne(value, master.getId)
                val di = detailRepository.setFieldTwo(value, detail.getId)
                (mi, di)
              }
            }
          } yield Result.SUCCESS
        case _ =>
          LOG.info("BUSINESS: updating skipped")
          Future.successful(Result.SUCCESS)
      }
    } yield result.name()
  }.recover {
    case e: Exception =>
      LOG.info(s"BUSINESS: ${Thread.currentThread.getName} $e", e)
      Result.FAIL.name()
  }

  private def isEntitiesShouldBeUpdated(m: SomeEntityMaster, d: SomeEntityDetail): Boolean = {
    timeProvider() % 2 == 0
  }

}

object BusinessServiceFP2 {

  trait EntityRepository[T] {
    def findById(id: Long): Option[T]

    def setFieldOne(value: String, id: Long): Int

    def setFieldTwo(value: String, id: Long): Int
  }

  type SetRollbackHandler = () => Unit

  trait Transactor {
    def runUnsafe[T](f: SetRollbackHandler => T): T
  }

  trait AsyncHttpClient {
    def fetchAndPrepareNewFieldValue(id: Long): Future[String]
  }

}
