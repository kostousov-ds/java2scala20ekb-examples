package examples02

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object FutureExamples1 {

  def main(args: Array[String]): Unit = {

    def timeout = Future {
      println(s"${Thread.currentThread().getName} ${System.currentTimeMillis()} start timer")
      Thread.sleep(100)
      println(s"${Thread.currentThread().getName} ${System.currentTimeMillis()} timeout")
      "timeout"
    }

    def work = Future {
      println(s"${Thread.currentThread().getName} ${System.currentTimeMillis()} start work")
      Thread.sleep(1000)
      println(s"${Thread.currentThread().getName} ${System.currentTimeMillis()} do some work")
      "data"
    }.map { data =>
      println(s"${Thread.currentThread().getName} ${System.currentTimeMillis()} fetched $data")
      "work done"
    }.flatMap { data =>
      Future {
        Thread.sleep(1000)
        println(s"${Thread.currentThread().getName} ${System.currentTimeMillis()} do some business")
        "business"
      }
    }

    def abandoned(f: Future[String]): Unit = {
      f.map { data =>
        println(s"${Thread.currentThread().getName} ${System.currentTimeMillis()} $data another map")
      }
      f.map { data =>
        println(s"${Thread.currentThread().getName} ${System.currentTimeMillis()} $data another map 2")
      }
    }

    println(s"${Thread.currentThread().getName} ${System.currentTimeMillis()} prepare")
    val timeoutF = timeout
    val workF = work

    abandoned(timeoutF)

    println(s"${Thread.currentThread().getName} ${System.currentTimeMillis()} begin")
    val f = Future.firstCompletedOf(List(timeoutF, workF))

    f.map(str => println(s"${Thread.currentThread().getName} ${System.currentTimeMillis()} final $str"))

    Thread.sleep(3000)
    println(s"${Thread.currentThread().getName} done")
  }

}
