package examples02

import scala.concurrent.{ExecutionContext, Future}

trait BusinessServiceFP0 {
import BusinessServiceFP0._

  def masterRepository: EntityRepository[SomeEntityMaster]

  def detailRepository: EntityRepository[SomeEntityDetail]

  def transactor: Transactor

  def asyncHttpClient: AsyncHttpClient

  def timeProvider: () => Long

  implicit def ec: ExecutionContext

  def doSomeBusiness(masterId: Long, detailId: Long): Future[String] = {
    val entitiesOptF = Future {
      transactor.runUnsafe { setRollbackHandler =>
        val masterOpt = masterRepository.findById(masterId)
        val detailOpt = detailRepository.findById(detailId)
        masterOpt.flatMap(m => detailOpt.map(d => (m, d)))
      }
    }

    val result = entitiesOptF.flatMap { pairOpt =>
      pairOpt.map { pair =>

        if (isEntitiesShouldBeUpdated(pair._1, pair._2)) {

          val newFieldValueF = asyncHttpClient.fetchAndPrepareNewFieldValue(pair._1.getId)

          val ret = newFieldValueF.map { value =>
            transactor.runUnsafe { setRollbackHandler =>
              val mi = masterRepository.setFieldOne(value, pair._1.getId)
              val di = detailRepository.setFieldTwo(value, pair._2.getId)
              (mi, di)
            }

          }.map(v => Result.SUCCESS)
          ret
        } else {
          Future.successful(Result.SUCCESS)
        }

      }.getOrElse(Future.successful(Result.ERROR))
    }.recover{
      case e: Exception => Result.FAIL
    }

    result.map(_.name())
  }

  private def isEntitiesShouldBeUpdated(m: SomeEntityMaster, d: SomeEntityDetail): Boolean = {
    timeProvider() % 2 == 0
  }

}

object BusinessServiceFP0 {

  trait EntityRepository[T] {
    def findById(id: Long): Option[T]

    def setFieldOne(value: String, id: Long): Int

    def setFieldTwo(value: String, id: Long): Int
  }

  type SetRollbackHandler = () => Unit

  trait Transactor {
    def runUnsafe[T](f: SetRollbackHandler => T): T
  }

  trait AsyncHttpClient {
    def fetchAndPrepareNewFieldValue(id: Long): Future[String]
  }

}
