package spring

case class SomeEntityMaster(id: Long){
  def getId: Long = id
  def setFieldOne(v: String): Unit = {}
}

case class SomeEntityDetail(id: Long) {
  def getId: Long = id
  def setFieldOne(v: String): Unit = {}
}

sealed trait Result{
  def name(): String
}
object Result{
  case object SUCCESS extends Result {
    override def name(): String = "SUCCESS"
  }

  case object FAIL extends Result {
    override def name(): String = "FAIL"
  }

  case object ERROR extends Result {
    override def name(): String = "ERROR"
  }
}